#!/bin/bash

# 配置镜像服务器 URL
MIRROR_URL="http://mirrors.aliyun.com/debian/"
MIRROR_SECURITY_URL="http://mirrors.aliyun.com/debian-security/"
PROXMOX_MIRROR_URL="https://mirrors.tuna.tsinghua.edu.cn/proxmox/debian"

# 检查是否在 Debian 系统上运行
if ! grep -q '^ID=debian' /etc/os-release; then
    echo "此脚本仅适用于 Debian 或 Proxmox 系统。"
    exit 1
else
    echo "当前系统为 Debian 系统"
fi

# 检查是否以 root 身份运行
if [ "$(id -u)" -ne 0 ]; then
    echo "请以 root 用户身份运行此脚本。"
    echo "您可以使用 'sudo' 命令运行此脚本，例如："
    echo "sudo $0"
    exit 1
fi

# 获取 Debian 的版本号
DEBIAN_VERSION=$(lsb_release -cs)
if [ -z "$DEBIAN_VERSION" ]; then
    # 如果 lsb_release 不可用，则尝试从 /etc/os-release 获取版本信息
    echo "检测到 lsb_release 不可用，则尝试从 /etc/os-release 获取版本信息"
    DEBIAN_VERSION=$(grep -E '^VERSION_CODENAME=' /etc/os-release | cut -d'=' -f2 | tr -d '"')
fi

# 检查是否成功获取版本号
if [ -z "$DEBIAN_VERSION" ]; then
    echo "无法确定 Debian 的版本号。"
    exit 1
else
    echo "检测到 Debian 版本为: $DEBIAN_VERSION"
fi

# 检查 Debian 版本是否为 Bullseye 或 Bookworm
if [[ "$DEBIAN_VERSION" != "bullseye" && "$DEBIAN_VERSION" != "bookworm" ]]; then
    echo "此脚本仅适用于 Debian Bullseye 或 Bookworm。"
    exit 1
fi

# 备份原始的 sources.list 文件
echo "正在备份原始的 sources.list 文件..."
cp /etc/apt/sources.list /etc/apt/sources.list.bak.$(date +%Y%m%d%H%M%S)
if [ $? -ne 0 ]; then
    echo "备份 sources.list 文件失败。"
    exit 1
else
    echo "备份完成。"
fi

# 清空 sources.list 文件
echo "清空 sources.list 文件..."
> /etc/apt/sources.list
if [ $? -ne 0 ]; then
    echo "清空 sources.list 文件失败。"
    exit 1
else
    echo "清空 sources.list 文件完成。"
fi

# 写入新的镜像源
echo "正在写入新的镜像源..."
cat <<EOF >> /etc/apt/sources.list
deb $MIRROR_URL $DEBIAN_VERSION main contrib non-free non-free-firmware
#deb-src $MIRROR_URL $DEBIAN_VERSION main contrib non-free non-free-firmware
deb $MIRROR_URL $DEBIAN_VERSION-updates main contrib non-free non-free-firmware
#deb-src $MIRROR_URL $DEBIAN_VERSION-updates main contrib non-free non-free-firmware
deb $MIRROR_URL $DEBIAN_VERSION-backports main contrib non-free non-free-firmware
#deb-src $MIRROR_URL $DEBIAN_VERSION-backports main contrib non-free non-free-firmware
deb $MIRROR_SECURITY_URL $DEBIAN_VERSION-security main 
#deb-src $MIRROR_SECURITY_URL $DEBIAN_VERSION-security main
EOF

if [ $? -ne 0 ]; then
    echo "写入新的镜像源失败。"
    exit 1
else
    echo "写入完成。"
fi

# 检查是否为 Proxmox 系统
IS_PVE=0
if [ -d "/etc/pve" ]; then
    IS_PVE=1
elif dpkg -l | grep -qw proxmox-ve; then
    IS_PVE=1
fi

if [ $IS_PVE -eq 1 ]; then
    echo "检测到当前系统为 Proxmox VE。"
else
    echo "检测到当前系统为普通 Debian 系统。"
fi

# 如果是 Proxmox 系统，则添加 Proxmox 的源
if [ $IS_PVE -eq 1 ]; then
    # 备份原始的 ceph.list 文件
    echo "正在备份原始的 ceph.list 文件..."
    cp /etc/apt/sources.list.d/ceph.list /etc/apt/ceph.list.bak.$(date +%Y%m%d%H%M%S)

    # 清空 ceph.list 文件
    echo "清空 ceph.list 文件..."
    > /etc/apt/sources.list.d/ceph.list

    if [ $? -ne 0 ]; then
        echo "清空 ceph.list 文件失败。"
        exit 1
    else
        echo "清空 ceph.list 文件完成。"
    fi

    echo "正在添加 Proxmox ceph 的源..."

    cat <<EOF >> /etc/apt/sources.list.d/ceph.list
    deb $PROXMOX_MIRROR_URL/ceph-quincy $DEBIAN_VERSION no-subscription
EOF

    if [ $? -ne 0 ]; then
        echo "写入 Proxmox ceph 的源失败。"
        exit 1
    else
        echo "写入 Proxmox ceph 的源完成。"
    fi

    # 备份原始的 pve-enterprise.list 文件
    echo "正在备份原始的 pve-enterprise.list 文件..."
    cp /etc/apt/sources.list.d/pve-enterprise.list /etc/apt/pve-enterprise.list.bak.$(date +%Y%m%d%H%M%S)

    # 清空 pve-enterprise.list 文件
    echo "清空 pve-enterprise.list 文件..."
    > /etc/apt/sources.list.d/pve-enterprise.list

    if [ $? -ne 0 ]; then
        echo "清空 pve-enterprise.list 文件失败。"
        exit 1
    else
        echo "清空 pve-enterprise.list 文件完成。"
    fi

    echo "正在添加 Proxmox pve-enterprise 的源..."

    cat <<EOF >> /etc/apt/sources.list.d/pve-enterprise.list
    deb $PROXMOX_MIRROR_URL $DEBIAN_VERSION pve-no-subscription
EOF

    if [ $? -ne 0 ]; then
        echo "写入 Proxmox pve-enterprise 的源失败。"
        exit 1
    else
        echo "写入 Proxmox pve-enterprise 的源完成。"
    fi

    #删除订阅弹窗
    sed -Ezi.bak "s/(Ext.Msg.show\(\{\s+title: gettext\('No valid sub)/void\(\{ \/\/\1/g" /usr/share/javascript/proxmox-widget-toolkit/proxmoxlib.js
    echo "0 1 * * * sed -Ezi \"s/(Ext.Msg.show\(\{\s+title: gettext\('No valid sub)/void\(\{ \/\/\1/g\" /usr/share/javascript/proxmox-widget-toolkit/proxmoxlib.js" | crontab -

    #设置Web控制台默认语言为中文
    echo 'language: zh_CN' >>/etc/pve/datacenter.cfg

    # 重启相关服务
    systemctl restart pvedaemon & systemctl restart pveproxy.service
fi

# 更新 APT 包索引
echo "正在更新 APT 包索引..."
apt-get update
if [ $? -ne 0 ]; then
    echo "APT 包索引更新失败。"
    exit 1
else
    echo "APT 包索引更新完成。"
fi

echo "Debian/PVE 源更换完成。"